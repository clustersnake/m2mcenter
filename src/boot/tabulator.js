// import something here
import VueTabulator from 'vue-tabulator';

// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/cli-documentation/boot-files#Anatomy-of-a-boot-file
export default async ({ /* app, router, */Vue } ) => {
  // something to do
  Vue.use(VueTabulator);
}
